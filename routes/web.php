<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@daft');

Route::post('/kir', 'AuthController@slm');

//crud cast
//ke form create data
Route::get('/cast/create','CastController@create');
//simpan data ka tabel
Route::post('/cast','CastController@store');

//tampil all data
Route::get('/cast','CastController@index');
//detail data
Route::get('/cast/{cast_id}','CastController@show');

//ke form edit data
Route::get('/cast/{cast_id}/edit','CastController@edit');
//update data ke tabel cast
Route::put('/cast/{cast_id}','CastController@update');
//delete
Route::delete('/cast/{cast_id}','CastController@destroy');